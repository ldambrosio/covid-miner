#!/bin/bash
# WORKFLOW for Spike Protein consensus building

set -e


fasta=$1
echo "fasta file: $fasta"

base=$(basename $fasta | sed -r 's/gisaid_cov2020_//' | sed -r 's/_sequences//')
echo "running with ${base%.*} fasta sequences"

sampleDir="/data/sequence/covid-19/results/${base%.*}"
mkdir -p $sampleDir

datetime=$(eval "date +"%d_%m_%y-%H_%M"") 
exec > >(tee -i $sampleDir/${datetime}.log)
exec 2>&1

cd $sampleDir

#-----step 1

# Bowtie2 alignment of GISAID COVID-19 sequences, divided 
# in European, Chinese, and US strains, to Wuhan reference

bowtie2 -p 4 -x /data/sequence/covid-19/covid-19/ref/covid-reference -f $fasta -S ${base%.*}_align.sam --verbose
samtools view -Sb  ${base%.*}_align.sam  >  ${base%.*}_align.bam
samtools sort ${base%.*}_align.bam >  ${base%.*}_align_srt.bam


#-----step 2

# split the bam file per read so to run mpileup with only 1 read per bam
> ${base%.*}_bam_list.tsv
samtools view -F4 ${base%.*}_align_srt.bam | while read A; do name=$(echo "$A"|cut -f 2 -d "|"); echo "$name"; echo "$name".bam >> ${base%.*}_bam_list.tsv; (samtools view -H ${base%.*}_align_srt.bam && echo "$A" )| samtools view -bS - > "$name".bam; done

ulimit -n 25500

# multi-sample variant calling
# by taking the advantage of correlation between samples, multi-sample variant calling makes it possible to
# find mutations at low coverage sites
bcftools mpileup -Q 5 -Oz -a "AD,ADF,ADR,INFO/AD,INFO/ADF,INFO/ADR" -f /data/sequence/covid-19/covid-19/ref/covid-reference.fasta -b ${base%.*}_bam_list.tsv > ${base%.*}_bcftools_mpileup_output.vcf.gz

rm EPI_ISL_*.bam

bcftools call -P 0.05 --ploidy 1 -mv -Oz ${base%.*}_bcftools_mpileup_output.vcf.gz -o ${base%.*}_calls.vcf.gz

# normalize indels
bcftools norm -f /data/sequence/covid-19/covid-19/ref/covid-reference.fasta -Oz ${base%.*}_calls.vcf.gz -o ${base%.*}_calls.norm.vcf.gz


#-----step 3

# downstream bcftools consensus creates consensus sequence by applying VCF variants to a reference fasta.
# It does not output the consensus base (most frequent allele) at each position, always applying the ALT
# allele to the reference fasta, ignoring the depths of the REF and ALT allele.

# This command includes records for which the depth of the ALT allele exceeds the REF allele
bcftools filter -i '(DP4[0]+DP4[1]) < (DP4[2]+DP4[3])' ${base%.*}_calls.norm.vcf.gz > ${base%.*}_calls_pass_variants.vcf 

bgzip -c ${base%.*}_calls_pass_variants.vcf > ${base%.*}_calls_pass_variants.vcf.gz
bcftools index ${base%.*}_calls_pass_variants.vcf.gz


#-----step 4

# complete covid genome consensus building 
bcftools consensus -f /data/sequence/covid-19/covid-19/ref/covid-reference.fasta ${base%.*}_calls_pass_variants.vcf.gz -o ${base%.*}_covid_consensus_sequence.fa

# spike sequence consensus building 
samtools faidx /data/sequence/covid-19/covid-19/ref/covid-reference.fasta NC_045512.2:21563-25384 | bcftools consensus ${base%.*}_calls_pass_variants.vcf.gz -o ${base%.*}_consensus_spike_sequence.fa

#------step 3.1
# variant annotation via snpEff
snpEff NC_045512.2 ${base%.*}_calls.norm.vcf.gz -canon > ${base%.*}_calls_snpeff.vcf  

#parse annotated vcf to table
java -jar /opt/miniconda2/share/snpeff-4.5covid19-1/SnpSift.jar extractFields -s "," -e "." ${base%.*}_calls_snpeff.vcf CHROM POS REF ALT "ANN[0].ANNOTATION" "ANN[0].GENE" "ANN[0].HGVS_C" "ANN[0].HGVS_P" DP ADF ADR AD > ${base%.*}_calls_snpsift.tsv

#to have one effect per line use the vcfEffOnePerLine.pl provided with SnpEff distribution
#cat ${base%.*}_calls_snpeff.vcf | /opt/miniconda2/share/snpeff-4.5covid19-1/scripts/vcfEffOnePerLine.pl | java -jar /opt/miniconda2/share/snpeff-4.5covid19-1/SnpSift.jar extractFields -e "." - CHROM POS REF ALT "ANN[*].ANNOTATION" "ANN[*].GENE" "ANN[*].HGVS_C" "ANN[*].HGVS_P" DP ADF ADR AD > ${base%.*}_calls_snpsift_one_per_line.csv

#TODO create binary matrix to count occurrences 
