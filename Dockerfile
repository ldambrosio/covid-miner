FROM epruesse/biocondabot

RUN conda install --yes bowtie2 samtools bcftools snpEff snpsift

RUN mkdir -p /data/sequence/covid-19/covid-19 /data/sequence/covid-19/testing

# Temporary fixes
RUN ln -s /opt/conda /opt/miniconda2
RUN ln -s /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar /opt/miniconda2/share/snpeff-4.5covid19-1/SnpSift.jar

ENV MAIN_FOLDER="/data/sequence/covid-19/covid-19"
ENV MAIN_SCRIPT="agnostic_assembly_cov19_consensus_sequence.sh"

WORKDIR ${MAIN_FOLDER}

COPY ${MAIN_SCRIPT} /data/sequence/covid-19/covid-19/${MAIN_SCRIPT}
COPY ref /data/sequence/covid-19/covid-19/ref
COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN chmod +x /data/sequence/covid-19/covid-19/${MAIN_SCRIPT} /usr/local/bin/docker-entrypoint


ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
