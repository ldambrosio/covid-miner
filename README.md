# covid-19

Tools to build a consensus sequence for DNA vaccines.

# The workflow

Sequence consensus workflow to construct consensus sequence for covid19 by aligning GISAID nucleotide fasta sequences to the Wuhan reference, then determining the most frequently occurring nucleotide at each position. \
The workflow takes advantage of bcftools consensus to apply snp and indel variant calls to the Wuhan reference.

# Mutation frequencies script

The script will calculate mutation frequencies from a "snpsift" table.
Usage: Run the script with Rscript in an environment with R language installed (at least version 3.6.1) and "dplyr", "tidyr", "readr" libraries from the "tidyverse" package. Snpsift table must be tab separated. Results will be written in the current working directory by default.

Example: Rscript --vanilla frequencies_script.R table_snpsift.tsv
